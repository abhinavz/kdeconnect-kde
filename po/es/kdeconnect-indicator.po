# Spanish translations for kdeconnect-indicator.po package.
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Víctor Rodrigo Córdoba <vrcordoba@gmail.com>, %Y.
# Automatically generated, 2020.
# Eloy Cuadra <ecuadra@eloihr.net>, 2020.
# Víctor Rodrigo Córdoba <vrcordoba@gmail.com>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-indicator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-03 00:41+0000\n"
"PO-Revision-Date: 2023-08-12 17:27+0200\n"
"Last-Translator: Víctor Rodrigo Córdoba <vrcordoba@gmail.com>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Víctor Rodrigo Córdoba"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vrcordoba@gmail.com"

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr "Navegar dispositivo"

#: deviceindicator.cpp:67
#, kde-format
msgid "Send clipboard"
msgstr "Enviar portapapeles"

#: deviceindicator.cpp:81
#, kde-format
msgctxt "@action:inmenu play bell sound"
msgid "Ring device"
msgstr "Hacer sonar el dispositivo"

#: deviceindicator.cpp:97
#, kde-format
msgid "Send a file/URL"
msgstr "Enviar un archivo/URL"

#: deviceindicator.cpp:107
#, kde-format
msgid "SMS Messages..."
msgstr "Mensajes SMS..."

#: deviceindicator.cpp:120
#, kde-format
msgid "Run command"
msgstr "Ejecutar orden"

#: deviceindicator.cpp:122
#, kde-format
msgid "Add commands"
msgstr "Añadir órdenes"

#: indicatorhelper_mac.cpp:37
#, kde-format
msgid "Launching"
msgstr "Lanzando"

#: indicatorhelper_mac.cpp:87
#, kde-format
msgid "Launching daemon"
msgstr "Lanzando demonio"

#: indicatorhelper_mac.cpp:96
#, kde-format
msgid "Waiting D-Bus"
msgstr "Esperando a D-Bus"

#: indicatorhelper_mac.cpp:113 indicatorhelper_mac.cpp:125
#: indicatorhelper_mac.cpp:144
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:114 indicatorhelper_mac.cpp:126
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"No se pudo conectar a DBus\n"
"KDE Connect se cerrará"

#: indicatorhelper_mac.cpp:144
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "No se pudo encontrar kdeconnectd"

#: indicatorhelper_mac.cpp:149
#, kde-format
msgid "Loading modules"
msgstr "Cargando módulos"

#: main.cpp:53
#, kde-format
msgid "KDE Connect Indicator"
msgstr "Indicador de KDE Connect"

#: main.cpp:55
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "Herramienta del indicador de KDE Connect"

#: main.cpp:57
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "© 2016 Aleix Pol Gonzalez"

#: main.cpp:92
#, kde-format
msgid "Configure..."
msgstr "Configurar..."

#: main.cpp:114
#, kde-format
msgid "Pairing requests"
msgstr "Peticiones de emparejamiento"

#: main.cpp:119
#, kde-format
msgctxt "Accept a pairing request"
msgid "Pair"
msgstr "Emparejar"

#: main.cpp:120
#, kde-format
msgid "Reject"
msgstr "Rechazar"

#: main.cpp:126 main.cpp:136
#, kde-format
msgid "Quit"
msgstr "Quitar"

#: main.cpp:155 main.cpp:179
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "%1 dispositivo conectado"
msgstr[1] "%1 dispositivos conectados"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "Sin batería"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Batería: %1% (Cargando)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "Batería: %1%"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr "No hay conexión con el teléfono"

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr "%1  | ~%2%"

#~ msgid "Get a photo"
#~ msgstr "Tomar una foto"
